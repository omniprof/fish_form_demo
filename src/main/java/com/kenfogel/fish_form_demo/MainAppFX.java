package com.kenfogel.fish_form_demo;

import com.kenfogel.fish_form_demo.beans.FishData;
import java.io.IOException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kenfogel.fish_form_demo.controllers.FishFormController;
import com.kenfogel.fish_form_demo.persistence.FishV2DAO;
import com.kenfogel.fish_form_demo.persistence.FishV2DAOImpl;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Basic class for starting a JavaFX application
 *
 * #KFCStandard and JavaFX8
 *
 * @author Ken Fogel
 */
public class MainAppFX extends Application {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(MainAppFX.class);

    // The primary window or frame of this application
    private Stage primaryStage;

    private final FishV2DAO fishDAO;
    private final FishData fishData;

    /**
     * Constructor
     */
    public MainAppFX() {
        super();
        fishDAO = new FishV2DAOImpl();
        fishData = new FishData();
    }

    /**
     * The application starts here
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {

        LOG.info("Program Begins");

        // The Stage comes from the framework so make a copy to use elsewhere
        this.primaryStage = primaryStage;
        // Create the Scene and put it on the Stage
        configureStage();

        // Set the window title
        primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("TITLE"));
        // Raise the curtain on the Stage
        primaryStage.show();
    }

    /**
     * Load the FXML and bundle, create a Scene and put the Scene on Stage.
     *
     * Using this approach allows you to use loader.getController() to get a
     * reference to the fxml's controller should you need to pass data to it.
     * Not used in this archetype.
     */
    private void configureStage() {
        try {
            // Instantiate the FXMLLoader
            FXMLLoader loader = new FXMLLoader();

            // Set the location of the fxml file in the FXMLLoader
            loader.setLocation(MainAppFX.class.getResource("/fxml/FishForm.fxml"));

            // Localize the loader with its bundle
            // Uses the default locale and if a matching bundle is not found
            // will then use MessagesBundle.properties
            loader.setResources(ResourceBundle.getBundle("MessagesBundle"));

            // Parent is the base class for all nodes that have children in the
            // scene graph such as AnchorPane and most other containers
            Parent parent = (GridPane) loader.load();

            // Load the parent into a Scene
            Scene scene = new Scene(parent);

            // Put the Scene on Stage
            primaryStage.setScene(scene);

            // Retrieve a reference to the controller so that you can pass a
            // reference to the persistence object and the bean
            FishFormController controller = loader.getController();

            controller.setFishDAOData(fishData, fishDAO);

        } catch (IOException ex) { // getting resources or files
            // could fail
            LOG.error(null, ex);
            errorAlert(ex.getMessage());
            System.exit(1);
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("ErrorStartTitle"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessagesBundle").getString("ErrorStartText"));
        dialog.setContentText(msg);
        dialog.show();
    }

    /**
     * Where it all begins
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
