package com.kenfogel.fish_form_demo.tests;

import com.kenfogel.fish_form_demo.beans.FishData;
import com.kenfogel.fish_form_demo.persistence.FishV2DAO;
import com.kenfogel.fish_form_demo.persistence.FishV2DAOImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

/**
 * There are no tests in this sample project.
 *
 * @author Ken Fogel
 *
 */
public class FishV2DAOTest {

    // A Rule is implemented as a class with methods that are associared
    // with the lifecycle of a unit test. These methods run when required.
    // Avoids the need to cut and paste code into every test method.
    @Rule
    public MethodLogger methodLogger = new MethodLogger();

    // This is my local MySQL server
    private final String url = "jdbc:mysql://localhost:3306/AQUARIUM?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final String user = "fish";
    private final String password = "kfstandard";

    private FishData fishData1;
    private FishV2DAO fd;

    @Before
    public void initializeFishData() {
        fishData1 = new FishData(6, "African Brown Knife", "Xenomystus nigri", "6.0-8.0", "5-19 dH", "72-78F",
                "12 in TL", "Africa", "", "", "Carnivore");
        fd = new FishV2DAOImpl();
    }

    /**
     * Verify that after record 6 is record 7
     *
     * @throws SQLException
     */
    @Test
    public void testNextID() throws SQLException {
        FishData fishData2 = fd.findNextByID(fishData1);
        assertEquals("Record after 6 should be 7: ", fishData2.getId(), 7);
    }

    /**
     * Verify that before record 6 is record 5
     *
     * @throws SQLException
     */
    @Test
    public void testPrevID() throws SQLException {
        FishData fishData2 = fd.findPrevByID(fishData1);
        assertEquals("Record before 6 should be 5: ", fishData2.getId(), 5);
    }

    /**
     * This routine recreates the database for every test. This makes sure that
     * a destructive test will not interfere with any other test.
     *
     * This routine is courtesy of Bartosz Majsak, an Arquillian developer at
     * JBoss who helped me out last winter with an issue with Arquillian. Look
     * up Arquillian to learn what it is.
     */
    @Before
    public void seedDatabase() {
        final String seedDataScript = loadAsString("createFishTable.sql");
        try (Connection connection = DriverManager.getConnection(url, user, password);) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }
}
