package com.kenfogel.fish_form_demo.persistence;

import java.sql.SQLException;

import com.kenfogel.fish_form_demo.beans.FishData;

/**
 * Interface for CRUD methods
 *
 * @author Ken
 */
public interface FishV2DAO {

    public FishData findPrevByID(FishData fishData) throws SQLException;

    public FishData findNextByID(FishData fishData) throws SQLException;

    public int saveFish(FishData fishData) throws SQLException;
}
