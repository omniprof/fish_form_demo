package com.kenfogel.fish_form_demo.controllers;

import com.kenfogel.fish_form_demo.beans.FishData;
import com.kenfogel.fish_form_demo.persistence.FishV2DAO;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML controller for FishForm
 *
 * @author Ken Fogel
 */
public class FishFormController {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(FishFormController.class);
    
    private FishV2DAO fishDAO;
    private FishData fishData;

    @FXML
    private ResourceBundle resources;

    @FXML
    private TextField idTextField;

    @FXML
    private TextField commonNameTextField;

    @FXML
    private TextField latinTextField;

    @FXML
    private TextField phTextField;

    @FXML
    private TextField khTextField;

    @FXML
    private TextField tempTextField;

    @FXML
    private TextField fishSizeTextField;

    @FXML
    private TextField speciesOriginTextField;

    @FXML
    private TextField tankSizeTextField;

    @FXML
    private TextField stockingTextField;

    @FXML
    private TextField dietTextField;

    /**
     * Default/no param constructor
     */
    public FishFormController() {
        super();
    }

    /**
     * This method is automatically called after the fxml file has been loaded.
     * This code binds the properties of the data bean to the JavaFX controls.
     * Changes to a control is immediately written to the bean and a change to
     * the bean is immediately shown in the control.
     */
    @FXML
    private void initialize() {
        idTextField.setEditable(false);

        // Listen character by character
        dietTextField.textProperty().addListener(this::listenForDietChange);
        // Determine if focus is lost
        dietTextField.focusedProperty().addListener(this::listenForDietFocus);
    }

    /**
     * The FishData object is created in MainAppFX and passed into this class.
     * The bindings cannot occur until after the FishData object is received.
     * This method will be called once the FishData objec t is delivered.
     */
    private void doBindings() {

        // Two way binding
        Bindings.bindBidirectional(idTextField.textProperty(), fishData.idProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(commonNameTextField.textProperty(), fishData.commonNameProperty());
        Bindings.bindBidirectional(latinTextField.textProperty(), fishData.latinProperty());
        Bindings.bindBidirectional(phTextField.textProperty(), fishData.phProperty());
        Bindings.bindBidirectional(khTextField.textProperty(), fishData.khProperty());
        Bindings.bindBidirectional(tempTextField.textProperty(), fishData.tempProperty());
        Bindings.bindBidirectional(fishSizeTextField.textProperty(), fishData.fishSizeProperty());
        Bindings.bindBidirectional(speciesOriginTextField.textProperty(), fishData.speciesOriginProperty());
        Bindings.bindBidirectional(tankSizeTextField.textProperty(), fishData.tankSizeProperty());
        Bindings.bindBidirectional(stockingTextField.textProperty(), fishData.stockingProperty());
        Bindings.bindBidirectional(dietTextField.textProperty(), fishData.dietProperty());

        // One Way Binding. Bind from bean property to TextField
//        idTextField.textProperty().bind(fishData.idProperty().asString());
//        commonNameTextField.textProperty().bind(fishData.commonNameProperty());
//        latinTextField.textProperty().bind(fishData.latinProperty());
//        phTextField.textProperty().bind(fishData.phProperty());
//        khTextField.textProperty().bind(fishData.khProperty());
//        tempTextField.textProperty().bind(fishData.tempProperty());
//        fishSizeTextField.textProperty().bind(fishData.fishSizeProperty());
//        speciesOriginTextField.textProperty().bind(fishData.speciesOriginProperty());
//        tankSizeTextField.textProperty().bind(fishData.tankSizeProperty());
//        stockingTextField.textProperty().bind(fishData.stockingProperty());
//        dietTextField.textProperty().bind(fishData.dietProperty());
    }

    /**
     * This is an example of a change listener for a TextField. It is called for
     * every change, character by character. It can extract the reference to the
     * TextField control. ChangeListener validation is coupled to a field we
     * already know so rather than discovering what the control is we refer to
     * it directly in the code.
     *
     * @param observable
     * @param oldValue
     * @param newValue
     */
    private void listenForDietChange(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        if (newValue.equalsIgnoreCase("Moose")) {
            dietTextField.setText("No Mooses Allowed - ChangeListener");
            LOG.info("Found a Moose.");

            // Here is the code that determines which control changed. Useful
            // when a test for change is identical for all fields. Otherwise you
            // should have a method for each field.
            // Cast to the property type for the control
//            StringProperty textProperty = (StringProperty) observable;
            // Retrieve a reference to the control that the property belongs to
//            TextField textField = (TextField) textProperty.getBean();
//            textField.setText(oldValue);
        }
    }

    /**
     * This is an example of an InvalidationListener for a TextField. It is
     * called when the control it has been added to either gains or loses focus.
     * Unlike a ChangeListener it cannot discover the control it is added to
     * therefore we explicitly reference the control. Nor does it support new
     * and old values.
     *
     * @param observable
     */
    private void listenForDietFocus(Observable observable) {
        if (!dietTextField.isFocused()) {
            if (dietTextField.getText().equalsIgnoreCase("Beaver")) {
                dietTextField.setText("No Beavers Allowed - InvalidationListener");
            }
            LOG.info("Lost Focus");
        } else {
            LOG.info("Gain Focus");

        }
    }

    /**
     * Exit event handler
     *
     * @param event
     */
    @FXML
    void exitFish(ActionEvent event) {
        Platform.exit();
    }

    /**
     * Advance to the next fish in the table
     *
     * @param event
     */
    @FXML
    void nextFish(ActionEvent event) {
        try {
            fishDAO.findNextByID(fishData);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    /**
     * Move to the previous record in the table
     *
     * @param event
     */
    @FXML
    void prevFish(ActionEvent event) {
        try {
            fishDAO.findPrevByID(fishData);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    @FXML
    void onClear(ActionEvent event) {
        fishData.setCommonName("");
        fishData.setDiet("");
        fishData.setFishSize("");
        fishData.setId(-1);
        fishData.setKh("");
        fishData.setPh("");
        fishData.setLatin("");
        fishData.setSpeciesOrigin("");
        fishData.setStocking("");
        fishData.setTankSize("");
        fishData.setTemp("");

    }

    @FXML
    void onSave(ActionEvent event) {

        try {
            int records = fishDAO.saveFish(fishData);
            LOG.info("Records updated or saved = " + records);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }

    }

    /**
     * Sets a reference to the FishData and FishDAO objects
     *
     * @param fishData
     * @param fishDAO
     */
    public void setFishDAOData(FishData fishData, FishV2DAO fishDAO) {
        this.fishData = fishData;
        doBindings();
        try {
            this.fishDAO = fishDAO;
            fishDAO.findNextByID(fishData);
        } catch (SQLException ex) {
            LOG.error("SQL Error", ex);
            errorAlert(ex.getMessage());
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessagesBundle").getString("ErrorDAOTitle"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessagesBundle").getString("ErrorDAOText"));
        dialog.setContentText(msg);
        dialog.show();
    }

}
